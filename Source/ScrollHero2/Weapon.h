// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperSpriteActor.h"
#include "WeaponTypeEnum.h"
#include "Weapon.generated.h"

class AProjectile;
/**
 * 
 */
UCLASS()
class SCROLLHERO2_API AWeapon : public APaperSpriteActor
{
	GENERATED_BODY()

public:
	void OnFire();

	EWeaponTypeEnum GetWeaponType() { return WeaponType; }

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Setup)
	float Damage = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Setup)
	int Ammo = 0;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly, Category = Weapon)
	TEnumAsByte<EWeaponTypeEnum> WeaponType;


};
