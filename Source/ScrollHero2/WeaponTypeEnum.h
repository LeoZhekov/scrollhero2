// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * WeaponTypeEnum
 */
UENUM(BlueprintType)
enum EWeaponTypeEnum 
{
	None UMETA(DisplayName = "None"),
	Pistol UMETA(DisplayName = "Pistol"),
	Rifle UMETA(DisplayName = "Rifle"),
	Shotgun UMETA(DisplayName = "Shotgun"),
	Melee UMETA(DisplayName = "Melee")
};
