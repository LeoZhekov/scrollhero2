// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ScrollHero2 : ModuleRules
{
	public ScrollHero2(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Paper2D" });
	}
}
