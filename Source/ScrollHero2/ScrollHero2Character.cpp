// Copyright Epic Games, Inc. All Rights Reserved.

#include "ScrollHero2Character.h"
#include "PaperFlipbookComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "PaperFlipbook.h"
#include "Camera/CameraComponent.h"
#include "Weapon.h"

DEFINE_LOG_CATEGORY_STATIC(SideScrollerCharacter, Log, All);

//////////////////////////////////////////////////////////////////////////
// AScrollHero2Character

AScrollHero2Character::AScrollHero2Character()
{
  // Use only Yaw from the controller and ignore the rest of the rotation.
  bUseControllerRotationPitch = false;
  bUseControllerRotationYaw = true;
  bUseControllerRotationRoll = false;

  // Set the size of our collision capsule.
  GetCapsuleComponent()->SetCapsuleHalfHeight(96.0f);
  GetCapsuleComponent()->SetCapsuleRadius(40.0f);

  // Create a camera boom attached to the root (capsule)
  CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
  CameraBoom->SetupAttachment(RootComponent);
  CameraBoom->TargetArmLength = 500.0f;
  CameraBoom->SocketOffset = FVector(0.0f, 0.0f, 75.0f);
  CameraBoom->SetUsingAbsoluteRotation(true);
  CameraBoom->bDoCollisionTest = false;
  CameraBoom->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));


  // Create an orthographic camera (no perspective) and attach it to the boom
  SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
  SideViewCameraComponent->ProjectionMode = ECameraProjectionMode::Orthographic;
  SideViewCameraComponent->OrthoWidth = 2048.0f;
  SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);

  // Prevent all automatic rotation behavior on the camera, character, and camera component
  CameraBoom->SetUsingAbsoluteRotation(true);
  SideViewCameraComponent->bUsePawnControlRotation = false;
  SideViewCameraComponent->bAutoActivate = true;
  GetCharacterMovement()->bOrientRotationToMovement = false;

  // Configure character movement
  GetCharacterMovement()->GravityScale = 2.0f;
  GetCharacterMovement()->AirControl = 0.80f;
  GetCharacterMovement()->JumpZVelocity = 1000.f;
  GetCharacterMovement()->GroundFriction = 3.0f;
  GetCharacterMovement()->MaxWalkSpeed = 600.0f;
  GetCharacterMovement()->MaxFlySpeed = 600.0f;

  // Lock character motion onto the XZ plane, so the character can't move in or out of the screen
  GetCharacterMovement()->bConstrainToPlane = true;
  GetCharacterMovement()->SetPlaneConstraintNormal(FVector(0.0f, -1.0f, 0.0f));

  // Behave like a traditional 2D platformer character, with a flat bottom instead of a curved capsule bottom
  // Note: This can cause a little floating when going up inclines; you can choose the tradeoff between better
  // behavior on the edge of a ledge versus inclines by setting this to true or false
  GetCharacterMovement()->bUseFlatBaseForFloorChecks = true;


  // Enable replication on the Sprite component so animations show up when networked
  GetSprite()->SetIsReplicated(true);

  bReplicates = true;
}

void AScrollHero2Character::Tick(float DeltaSeconds)
{
  Super::Tick(DeltaSeconds);

  UpdateCharacter();
}

void AScrollHero2Character::BeginPlay()
{
  Super::BeginPlay();
  if (WeaponBlueprint != nullptr) {
    Weapon = GetWorld()->SpawnActor<AWeapon>(WeaponBlueprint);
    Weapon->AttachToComponent(GetSprite(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false), TEXT("WeaponPoint"));
  }
  GetSprite()->SetFlipbook(IdleAnimation);
}

//////////////////////////////////////////////////////////////////////////
// Animation

void AScrollHero2Character::UpdateAnimation(bool bIsShooting, EWeaponTypeEnum WeaponType)
{
 
  UPaperFlipbook* DesiredAnimation = ExtractAnimation(bIsShooting, WeaponType);
  if (GetSprite()->GetFlipbook() != DesiredAnimation)
  {
    GetSprite()->SetFlipbook(DesiredAnimation);
  }
}

UPaperFlipbook* AScrollHero2Character::ExtractAnimation(bool bIsShooting, EWeaponTypeEnum WeaponType)
{
  const FVector PlayerVelocity = GetVelocity();
  const float PlayerSpeedSqr = PlayerVelocity.SizeSquared();
  // Are we moving or standing still?
  UPaperFlipbook* DesiredAnimation = nullptr;
  if (bIsShooting) {
    // todo add more shooting animations.
    switch (WeaponType) {
    case Pistol:
    case Rifle:
    case Melee:
    case Shotgun:
      DesiredAnimation = PistolShootingAnimation;
      break;
    default:
      DesiredAnimation = (PlayerSpeedSqr > 0.0f) ? RunningAnimation : IdleAnimation;
    }
  }
  else
  {
    DesiredAnimation = (PlayerSpeedSqr > 0.0f) ? RunningAnimation : IdleAnimation;
  }
  return DesiredAnimation;
}


//////////////////////////////////////////////////////////////////////////
// Input

void AScrollHero2Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
  PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
  PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
  PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AScrollHero2Character::FireWeapon);
  PlayerInputComponent->BindAction("Fire", IE_Released, this, &AScrollHero2Character::StopFiring);

  PlayerInputComponent->BindAxis("MoveRight", this, &AScrollHero2Character::MoveRight);
}

void AScrollHero2Character::FireWeapon()
{
  if (Weapon != nullptr) {
    bIsCharacterShooting = true;
    UpdateAnimation(true, Weapon->GetWeaponType());
    Weapon->OnFire();
  }
  else {
    UE_LOG(LogTemp, Warning, TEXT("Yo, no weapon"))
  }
}

void AScrollHero2Character::StopFiring()
{
  bIsCharacterShooting = false;
  UpdateAnimation(false, EWeaponTypeEnum::None);
}

void AScrollHero2Character::MoveRight(float Value)
{
  /*UpdateChar();*/

  // Apply the input to the character motion
  if (!bIsCharacterShooting) {
    AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
    UpdateAnimation(false, EWeaponTypeEnum::None);
  }
}

void AScrollHero2Character::UpdateCharacter()
{
  // Update animation to match the motion
 // UpdateAnimation(false, EWeaponTypeEnum::None);

  // Now setup the rotation of the controller based on the direction we are travelling
  const FVector PlayerVelocity = GetVelocity();
  float TravelDirection = PlayerVelocity.X;
  // Set the rotation so that the character faces his direction of travel.
  if (Controller != nullptr)
  {
    if (TravelDirection < 0.0f)
    {
      Controller->SetControlRotation(FRotator(0.0, 180.0f, 0.0f));
    }
    else if (TravelDirection > 0.0f)
    {
      Controller->SetControlRotation(FRotator(0.0f, 0.0f, 0.0f));
    }
  }
}
