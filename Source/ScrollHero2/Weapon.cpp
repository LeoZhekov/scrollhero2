// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Projectile.h"
#include "PaperSpriteComponent.h"
#include "PaperSprite.h"


void AWeapon::OnFire()
{
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			if (ProjectileClass != nullptr) {
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				auto Projectile = World->SpawnActor<AProjectile>(ProjectileClass, GetRenderComponent()->GetSocketLocation(FName("FirePoint")), GetRenderComponent()->GetSocketRotation(FName("FirePoint")), ActorSpawnParams);
				Projectile->LaunchProjectile();
			}
		}
	}
}
