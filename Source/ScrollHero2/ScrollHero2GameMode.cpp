// Copyright Epic Games, Inc. All Rights Reserved.

#include "ScrollHero2GameMode.h"
#include "ScrollHero2Character.h"

AScrollHero2GameMode::AScrollHero2GameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = AScrollHero2Character::StaticClass();	
}
