// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "WeaponTypeEnum.h"
#include "ScrollHero2Character.generated.h"

class AWeapon;
/**
 * This class is the default character for ScrollHero2, and it is responsible for all
 * physical interaction between the player and the world.
 *
 * The capsule component (inherited from ACharacter) handles collision with the world
 * The CharacterMovementComponent (inherited from ACharacter) handles movement of the collision capsule
 * The Sprite component (inherited from APaperCharacter) handles the visuals
 */
UCLASS(config=Game)
class AScrollHero2Character : public APaperCharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta=(AllowPrivateAccess="true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;


	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
protected:

	// Animations
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Animations)
	class UPaperFlipbook* RunningAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* PistolShootingAnimation;

	/** Called to choose the correct animation to play based on the character's movement state and weather or not character is shooting and depending on the weapon type is holding */
	void UpdateAnimation(bool bIsShooting, EWeaponTypeEnum WeaponType);
	// End animations

	// Weapon
	AWeapon* Weapon;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<class AWeapon> WeaponBlueprint;

	void FireWeapon();
	void StopFiring();
	// End weapon

	/** Called for side to side input */
	void MoveRight(float Value);

	void UpdateCharacter();

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	AScrollHero2Character();

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }


private:
	UPaperFlipbook* ExtractAnimation(bool bIsShooting, EWeaponTypeEnum WeaponType);
	bool bIsCharacterShooting = false;
};
